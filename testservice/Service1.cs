﻿using log4net;
using System;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using Windows.Media.Control;
using WindowsMediaController;

namespace testservice
{
    public partial class Service1 : ServiceBase
    {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static MediaManager mediaManager;
        public System.Timers.Timer serviceTimer, updateTimer;
        public Service1()
        {
            InitializeComponent();
        }


        protected override void OnStart(string[] args)
        {
            try
            {
                serviceTimer = new System.Timers.Timer();
                updateTimer = new System.Timers.Timer();

                Log.Info("Service started!");

                serviceTimer.Elapsed += new ElapsedEventHandler(OnServiceElapsedTime);
                serviceTimer.Interval = 10000;
                serviceTimer.Enabled = true;
                serviceTimer.Start();

                runsongschecker();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        static void Main()
        {
#if (DEBUG)
            Service1 myService = new Service1();
            myService.OnDebug();
            
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            ServiceBase.Run(ServicesToRun);
#endif
            Thread.Sleep(Timeout.Infinite);
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        private void runsongschecker()
        {
            mediaManager = new MediaManager();
            mediaManager.OnAnySessionOpened += MediaManager_OnAnySessionOpened;
            mediaManager.OnAnySessionClosed += MediaManager_OnAnySessionClosed;
            mediaManager.OnAnyPlaybackStateChanged += MediaManager_OnAnyPlaybackStateChanged;
            mediaManager.OnAnyMediaPropertyChanged += MediaManager_OnAnyMediaPropertyChanged;
            mediaManager.Start();
        }

        private void OnServiceElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                // do some checks to see if service is still ok and log if problem
                Log.Info("Service still running.");
                return;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        private static void MediaManager_OnAnySessionOpened(MediaManager.MediaSession session)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        private static void MediaManager_OnAnySessionClosed(MediaManager.MediaSession session)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        private static void MediaManager_OnAnyPlaybackStateChanged(MediaManager.MediaSession sender, GlobalSystemMediaTransportControlsSessionPlaybackInfo args)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        private static void MediaManager_OnAnyMediaPropertyChanged(MediaManager.MediaSession sender, GlobalSystemMediaTransportControlsSessionMediaProperties args)
        {
            try
            {
                // called when song changes
                // check if previous timer is > 30s
                // if yes record song
                // else check for song
                // and start new timer
                Console.WriteLine("in media pro ch00");
                Log.Debug("status changed" + args.Title);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }

        }

        protected override void OnStop()
        {
        }
    }
}
