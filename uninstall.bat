@echo off

sc delete testservice

FOR /F "tokens=3" %%A IN ('sc queryex testservice ^| findstr PID') DO (SET pid=%%A)
 IF "%pid%" NEQ "0" (
  taskkill /F /PID %pid%
 )
 
pause